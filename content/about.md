+++ 
draft = false
date = 2020-05-07T15:08:19+08:00
title = "About Me"
slug = "about"
+++

Hey there, fellow tech enthusiast! 🚀 I'm Suha, a graduate in Computer Science with a specialization in network (but almost forgot them, almost zero again? Not of course, just average network guy that know less). My journey through the tech landscape was a bit like a rollercoaster—exploring mobile, web, and game programming during my college days.

Admittedly, those early years were a bit of a whirlwind. I found myself lost in the vast possibilities of tech, unsure of which path to tread. Mobile, web, game programming—I dipped my toes into each, trying to decipher where my true passion lay.

Amid the confusion, a moment of clarity emerged. The intricate dance of backend programming caught my attention, and suddenly, everything clicked. The elegance of weaving code behind the scenes, optimizing systems, and ensuring seamless operations became my guiding light.

Fast forward to today, I stand as a backend developer, with a particular fondness for the magic of Laravel. My passion extends beyond the lines of code, delving into the captivating worlds of anime and Korean dramas during my offtime.

Whether it's crafting efficient code or indulging in the latest tech trends, I'm always up for a good conversation. Join me in navigating the digital realm and uncovering the endless possibilities of technology!

Let's code, optimize, and explore together. 🌐💻✨