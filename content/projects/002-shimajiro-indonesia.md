+++ 
draft = false
date = 2019-12-07T15:08:19+08:00
title = "Shimajiro Indonesia"
description = "Shimajiro.id is the official website of Shimajiro Indonesia. The website features educational materials, such as videos, songs, and games, that are designed to help children learn important life skills in a fun and engaging way."
slug = "shimajiro-indonesia"
demoLink = "https://shimajiro.id"

[[images]]
    title = "Image 1"
    url = "https://cdn.suha.my.id/images/projects/shimajiro/shimajiro-01.jpg"
[[images]]
    title = "Image 2"
    url = "https://cdn.suha.my.id/images/projects/shimajiro/shimajiro-02.jpg"
[[rightImages]]
    title = "Image 3"
    url = "https://cdn.suha.my.id/images/projects/shimajiro/shimajiro-03.webp"
[[rightImages]]
    title = "Image 3"
    url = "https://cdn.suha.my.id/images/projects/shimajiro/shimajiro-04.webp"
[[techs]]
    name = "Laravel"
    image = "https://cdn.suha.my.id/images/techs/laravel.svg"
[[techs]]
    name = "VueJs"
    image = "https://cdn.suha.my.id/images/techs/vue.svg"
[[techs]]
    name = "MySQL"
    image = "https://cdn.suha.my.id/images/techs/mysql.svg"
[[techs]]
    name = "JQuery"
    image = "https://cdn.suha.my.id/images/techs/jquery.svg"
+++

Shimajiro.id is the official website of Shimajiro Indonesia. The website features educational materials, such as videos, songs, and games, that are designed to help children learn important life skills in a fun and engaging way. The website is available in both Indonesian and English and is targeted towards parents and caregivers of young children.

On the website, parents can find information about Shimajiro and his friends, as well as tips on how to use the educational materials to teach their children. There are also printable activities and worksheets that parents can use to reinforce the lessons taught in the videos and songs.

Overall, shimajiro.id is a valuable resource for parents and caregivers who are looking for fun and effective ways to teach their children important life skills.

Shimajiro Indonesia has closed their services permanently in July 2022. It's always sad to see a beloved educational service or resource close down, especially one that was designed to help children learn important life skills in a fun and engaging way.

I work as a backend programmer on this project. My responsibilities include preparing the APIs that will be used by the mobile app team and handling the frontend logic using Vue.
