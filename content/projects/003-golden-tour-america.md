+++ 
draft = false
date = 2020-05-07T15:08:19+08:00
title = "Golden Tour America"
description = "Designed to provide easy and convenient access to booking tours and guides throughout America."
slug = "golden-tour-america"
externalLink = ""

[[images]]
    title = "Image 1"
    url = "https://cdn.suha.my.id/images/projects/gta/1.jpg"
[[images]]
    title = "Image 2"
    url = "https://cdn.suha.my.id/images/projects/gta/2.jpg"
[[images]]
    title = "Image 2"
    url = "https://cdn.suha.my.id/images/projects/gta/3.jpg"
[[rightImages]]
    title = "Image 3"
    url = "https://cdn.suha.my.id/images/projects/gta/4.jpg"
[[rightImages]]
    title = "Image 3"
    url = "https://cdn.suha.my.id/images/projects/gta/5.jpg"
[[rightImages]]
    title = "Image 3"
    url = "https://cdn.suha.my.id/images/projects/gta/6.jpg"
[[techs]]
    name = "PHP"
    image = "https://cdn.suha.my.id/images/techs/php.svg"
[[techs]]
    name = "Bootstrap"
    image = "https://cdn.suha.my.id/images/techs/bootstrap.svg"
[[techs]]
    name = "MySQL"
    image = "https://cdn.suha.my.id/images/techs/mysql.svg"
[[techs]]
    name = "JQuery"
    image = "https://cdn.suha.my.id/images/techs/jquery.svg"
+++

Golden Tour America (goldentouramerica.com) is designed to provide easy and convenient access to booking tours and guides throughout America. With a user-friendly interface, visitors can easily browse through a wide selection of tours and guides that cater to different interests, budgets, and schedules.

The application features a comprehensive database of tour packages and guides, ranging from historical landmarks and museums to scenic routes and adventurous activities. Each listing provides detailed information about the tour, including its duration, cost, and itinerary. Visitors can also view photos and reviews from previous customers to help them make informed decisions.

However, due to the COVID-19 pandemic, the tourism industry has been severely impacted, and many tours and attractions have been closed or restricted. As a result, this project are inactive now.

This project created with Gurin (PHP framework).
