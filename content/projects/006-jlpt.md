+++ 
draft = false
date = 2022-12-10T15:08:19+08:00
title = "JLPT Indonesia"
description = "JLPT Online (Japanese Language Proficiency Test Online) is an official online Japanese language proficiency test, conducted by the Japan Foundation and Japan Educational Exchanges and Services. "
slug = "jlpt-indonesia"
externalLink = ""
demoLink = "https://jlptonline.or.id/"

[[images]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/1.png"
[[images]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/2.png"
[[images]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/3.png"
[[images]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/4.png"
[[images]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/5.png"
[[images]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/6.png"
[[images]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/7.png"
[[rightImages]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/8.png"
[[rightImages]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/9.png"
[[rightImages]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/10.png"
[[rightImages]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/11.png"
[[rightImages]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/12.png"
[[rightImages]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/13.png"
[[rightImages]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/jlpt/14.png"
[[techs]]
    name = "Laravel"
    image = "https://cdn.suha.my.id/images/techs/laravel.svg"
[[techs]]
    name = "VueJs"
    image = "https://cdn.suha.my.id/images/techs/vue.svg"
[[techs]]
    name = "MySQL"
    image = "https://cdn.suha.my.id/images/techs/mysql.svg"
[[techs]]
    name = "Bootstrap"
    image = "https://cdn.suha.my.id/images/techs/bootstrap.svg"
+++

JLPT Online (Japanese Language Proficiency Test Online) is an official online Japanese language proficiency test, conducted by the Japan Foundation and Japan Educational Exchanges and Services. The JLPT Online website, located at jlptonline.or.id, serves as the official registration portal for the test in Indonesia.

The JLPT is a standardized test that evaluates and certifies the Japanese language proficiency of non-native speakers. The test assesses four areas of language ability: reading, listening, writing, and speaking. The JLPT is widely recognized by companies and educational institutions in Japan and other countries as an official measure of Japanese language proficiency.

The JLPT Online test is available for levels N5, N4, and N3, and can be taken on a computer or mobile device with an internet connection. The test is conducted several times a year, and registration is done through the JLPT Online website.

In Indonesia, the JLPT Online test is gaining popularity, especially among students and professionals who wish to study or work in Japan. The JLPT Online website provides detailed information on the test format, registration procedure, and study materials to help test takers prepare for the exam.

In this project, I contributed on integrate available HTML and CSS code with the database and manage frontend logic with Vue.js. For tpayment as part of the main feature of this project, we used Midtrans Payment. Admin dashboard is created with Inertia (with Vue.js).