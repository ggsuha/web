+++ 
draft = false
date = 2022-12-07T15:08:19+08:00
title = "Personal Website"
description = "The project is this website itself. A platform where I want to showcase my works and projects."
slug = "personal-website"
externalLink = ""
demoLink = "https://suha.my.id/"

[[images]]
    title = "Image 1"
    url = "https://cdn.suha.my.id/images/projects/suha/1.jpg"
[[images]]
    title = "Image 2"
    url = "https://cdn.suha.my.id/images/projects/suha/2.jpg"
[[rightImages]]
    title = "Image 3"
    url = "https://cdn.suha.my.id/images/projects/suha/3.jpg"
[[rightImages]]
    title = "Image 3"
    url = "https://cdn.suha.my.id/images/projects/suha/4.jpg"
[[techs]]
    name = "Laravel"
    image = "https://cdn.suha.my.id/images/techs/laravel.svg"
[[techs]]
    name = "VueJs"
    image = "https://cdn.suha.my.id/images/techs/vue.svg"
[[techs]]
    name = "MySQL"
    image = "https://cdn.suha.my.id/images/techs/mysql.svg"
[[techs]]
    name = "Hugo"
    image = "https://cdn.suha.my.id/images/techs/hugo.svg"
+++

The project is this website itself. A platform where I want to showcase my works and projects. Each project has its own story, and I invite you to explore and discover them all.

Initially this project uses PHP Laravel as the backend language and Vue JS as the frontend language, but since it is such a pain since I'm not that active to manage the server, so I migrated to use Hugo.