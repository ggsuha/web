+++ 
draft = false
date = 2015-11-07T15:08:19+08:00
title = "CV. Sendiko Tour Travel"
description = "Online booking website for a travel agency in Yogyakarta created with Code Igniter 3."
slug = "sendiko-travel"
[[images]]
    title = "Image 1"
    url = "https://cdn.suha.my.id/images/projects/sendiko.png"

[[techs]]
    name = "Code Igniter"
    image = "https://cdn.suha.my.id/images/techs/codeigniter.svg"
[[techs]]
    name = "Bootstrap"
    image = "https://cdn.suha.my.id/images/techs/bootstrap.svg"
[[techs]]
    name = "JQuery"
    image = "https://cdn.suha.my.id/images/techs/jquery.svg"
[[techs]]
    name = "MySQL"
    image = "https://cdn.suha.my.id/images/techs/mysql.svg"
externalLink = ""
series = []
+++

Online booking website for a travel agency in Yogyakarta. The website is not operating anymore.
