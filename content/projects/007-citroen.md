+++ 
draft = false
date = 2023-01-01T15:08:19+08:00
title = "Citroën Booking"
description = "Citroenid-booking.com was designed as a website to assist dealers in collecting customer data for obtaining quotations or downloading brochures for new cars."
slug = "citroen-indonesia"
demoLink = "https://citroenid-booking.com/"

[[images]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/citroen/1.jpg"
[[rightImages]]
    title = "Image"
    url = "https://cdn.suha.my.id/images/projects/citroen/2.jpg"
[[techs]]
    name = "Laravel"
    image = "https://cdn.suha.my.id/images/techs/laravel.svg"
[[techs]]
    name = "VueJs"
    image = "https://cdn.suha.my.id/images/techs/vue.svg"
[[techs]]
    name = "MySQL"
    image = "https://cdn.suha.my.id/images/techs/mysql.svg"
+++

Citroenid-booking.com was designed as a website to assist dealers in collecting customer data for obtaining quotations or downloading brochures for new cars.